import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  loading;

  constructor(public loading_controller: LoadingController) { }

  async present() {
    this.loading = await this.loading_controller.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    this.loading.present();
  }

  async dismiss() {
    await this.loading.dismiss();
  }
}

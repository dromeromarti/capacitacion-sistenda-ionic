import { Component, OnInit } from '@angular/core';
import { CategoriasService } from '../servicios/categorias.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.page.html',
  styleUrls: ['./categorias.page.scss'],
})
export class CategoriasPage implements OnInit {
  public categorias;

  constructor(private categoriasService: CategoriasService,
    private router: Router,
) { }

  async ngOnInit() {
    this.categorias = await this.categoriasService.index().catch(error => console.log('este es el error', error));

    // Es un equivalente a la linea de arriba
    // await this.categoriasService.index().then((categorias) => {
    //   this.categorias = categorias;
    // }).catch(error => {
    //   console.log('este es el error', error);
    // });
  }
  async refreshView(){
    this.categorias = await this.categoriasService.index().catch(error => console.log('este es el error', error));
  }
  async editar(id){
   console.log(id);
   this.router.navigate(['categorias/' + id+'/editar']);

  }

  async eliminar(id){
   let res = await this.categoriasService.delete(id)
   console.log(res);
   this.refreshView();
  }

  async subcategoria(id){
    this.router.navigate(['categorias/' + id+'/subcategorias']);
  }

}

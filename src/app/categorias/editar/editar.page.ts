import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { CategoriasService } from 'src/app/servicios/categorias.service';
import { ToastService } from 'src/app/servicios/toast.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {
  id;
  objeto:any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private categorias_service:CategoriasService,
    private toastr_service:ToastService
  ) {
    this.objeto = {
      nombre:'',
      uri:'',
      total_subcategorias:'',
      created_at:'',
      updated_at:'',
      deleted_at:''
    }
  }

  async ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.objeto = await this.categorias_service.get(this.id);
    console.log(this.objeto);
  }

  async update(data){
    console.log(data)
    let res:any = await this.categorias_service.update(this.id,data)
    console.log(res);
    if(!res.res){
      return this.toastr_service.error(res.errores)
    }

    return this.toastr_service.success();

  }

}

import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { CategoriasService } from 'src/app/servicios/categorias.service';
import { ToastService } from 'src/app/servicios/toast.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  id;
  constructor(private categoriasService: CategoriasService,
    private toastService: ToastService) { }

  ngOnInit() {
    
    console.log(this.id);
  }

  async store(data) {
    const res: any = await this.categoriasService.store(data);
    if(!res.res) {
      console.log('res', res);
      this.toastService.error(res.errores);
      return;
    }

    this.toastService.success();
  }

}

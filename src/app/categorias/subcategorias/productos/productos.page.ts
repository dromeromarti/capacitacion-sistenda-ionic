import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/servicios/productos.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/servicios/toast.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.page.html',
  styleUrls: ['./productos.page.scss'],
})
export class ProductosPage implements OnInit {
  productos:any;
    id_categoria: string;
    id_subcategoria: string;

  constructor(
    private servicio_productos:ProductosService,
    private servicio_toast:ToastService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  async ngOnInit() {
    this.id_categoria = this.route.snapshot.paramMap.get('id');
    console.log(this.id_categoria);
    this.id_subcategoria = this.route.snapshot.paramMap.get('idSubcategoria');
    console.log(this.id_subcategoria);
    this.productos = await this.servicio_productos.index(this.id_subcategoria);
    console.log(this.productos)
  }

  async refreshView(){
    this.productos = await this.servicio_productos.index(this.id_subcategoria).catch(error => console.log('este es el error', error));
  }

  async agregar(){
    this.router.navigate(['categorias/' + this.id_categoria+'/subcategorias/'+this.id_subcategoria+'/productos/agregar']);

  }
  async editar(id){
   console.log(id);
   this.router.navigate(['categorias/' + this.id_categoria+'/subcategorias/'+this.id_subcategoria+'/productos/'+id+'/editar']);
  }

  async eliminar(id){
   let res = await this.servicio_productos.delete(id)
   this.servicio_toast.success();
   console.log(res);
   this.refreshView();
  }

}

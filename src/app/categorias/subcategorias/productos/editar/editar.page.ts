import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/servicios/productos.service';
import { ToastService } from 'src/app/servicios/toast.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {
  producto;
  id_producto;
  id_categoria;
  id_subcategoria;

  constructor(
    private servicio_productos:ProductosService,
    private servicio_toast:ToastService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.producto={
      nombre:'',
      subcategoria_id:'',
      precio:'',
      uri:'',
      descripcion:''
    }
  }

  async ngOnInit() {
    this.id_categoria = this.route.snapshot.paramMap.get('id');
    console.log(this.id_categoria);
    this.id_subcategoria = this.route.snapshot.paramMap.get('idSubcategoria');
    console.log(this.id_subcategoria);
    this.id_producto = this.route.snapshot.paramMap.get('idProducto');
    console.log(this.id_producto);

    this.producto = await this.servicio_productos.get(this.id_producto);
    console.log(this.producto)
  }

  async update(objeto){
    objeto.subcategoria_id = this.id_subcategoria;
    let resultado:any = await this.servicio_productos.update(this.id_producto,objeto);
    if(!resultado.res){
      console.log(resultado);
      this.servicio_toast.error(resultado.errores)
      return;
    }
    this.servicio_toast.success();
    this.router.navigate(['categorias/' + this.id_categoria+'/subcategorias/'+this.id_subcategoria+'/productos/']);

  }

}

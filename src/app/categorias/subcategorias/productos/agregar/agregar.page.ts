import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/servicios/productos.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/servicios/toast.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  id_categoria: string;
  id_subcategoria: string;
  constructor(
    private servicio_productos:ProductosService,
    private servicio_toast:ToastService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.id_categoria = this.route.snapshot.paramMap.get('id');
    console.log(this.id_categoria);
    this.id_subcategoria = this.route.snapshot.paramMap.get('idSubcategoria');
    console.log(this.id_subcategoria);
  }

  async store(data) {
    data.subcategoria_id = this.id_subcategoria;
    const res: any = await this.servicio_productos.store(data);
    if(!res.res) {
      console.log('res', res);
      this.servicio_toast.error(res.errores);
      return;
    }

    this.servicio_toast.success();
    this.router.navigate(['categorias/' + this.id_categoria+'/subcategorias/'+this.id_subcategoria+'/productos/']);

  }

}

import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { SubcategoriasService } from 'src/app/servicios/subcategorias.service';
import { ToastService } from 'src/app/servicios/toast.service';

@Component({
  selector: 'app-subcategorias',
  templateUrl: './subcategorias.page.html',
  styleUrls: ['./subcategorias.page.scss'],
})
export class SubcategoriasPage implements OnInit {
  categoria;
  id_categoria;
  subcategorias;
  constructor(
    private toastr_service:ToastService,
    private subcategorias_service:SubcategoriasService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  async ngOnInit() {
    this.id_categoria = this.route.snapshot.paramMap.get('id');
    console.log(this.id_categoria);
    this.subcategorias = await this.subcategorias_service.index(this.id_categoria);
    console.log(this.subcategorias);
  }

  async refreshView(){
    this.subcategorias = await this.subcategorias_service.index(this.id_categoria).catch(error => console.log('este es el error', error));
  }

  async agregar(){
    this.router.navigate(['categorias/' + this.id_categoria+'/subcategorias/agregar']);

  }
  async editar(id){
   console.log(id);
   this.router.navigate(['categorias/' + this.id_categoria+'/subcategorias/'+ id +'/editar']);
  }

  async eliminar(id){
   let res = await this.subcategorias_service.delete(id)
   console.log(res);
   this.refreshView();
  }

  async productos(id){
    this.router.navigate(['categorias/' + this.id_categoria+'/subcategorias/'+ id +'/productos']);

  }

}

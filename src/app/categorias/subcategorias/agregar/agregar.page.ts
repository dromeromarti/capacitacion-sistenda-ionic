import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { SubcategoriasService } from 'src/app/servicios/subcategorias.service';
import { ToastService } from 'src/app/servicios/toast.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  id
  constructor(private subcategoriasService: SubcategoriasService,
    private router: Router,
    private route: ActivatedRoute,
    private toastService: ToastService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

  }

  async store(data) {
    data.categoria_id = this.id;
    const res: any = await this.subcategoriasService.store(data);
    if(!res.res) {
      console.log('res', res);
      this.toastService.error(res.errores);
      return;
    }

    this.toastService.success();
  }



}

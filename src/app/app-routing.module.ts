import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'categorias',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'categorias',
    loadChildren: () => import('./categorias/categorias.module').then( m => m.CategoriasPageModule)
  },
  {
    path: 'categorias/agregar',
    loadChildren: () => import('./categorias/agregar/agregar.module').then( m => m.AgregarPageModule)
  },
  {
    path: 'categorias/:id/editar',
    loadChildren: () => import('./categorias/editar/editar.module').then( m => m.EditarPageModule)
  },
  {
    path: 'categorias/:id/subcategorias',
    loadChildren: () => import('./categorias/subcategorias/subcategorias.module').then( m => m.SubcategoriasPageModule)
  },
  {
    path: 'categorias/:id/subcategorias/:idSubcategoria/editar',
    loadChildren: () => import('./categorias/subcategorias/editar/editar.module').then( m => m.EditarPageModule)
  },
  {
    path: 'ejemplo',
    loadChildren: () => import('./ejemplo/ejemplo.module').then( m => m.EjemploPageModule)
  },
  {
    path: 'categorias/:id/subcategorias/:idSubcategoria/productos',
    loadChildren: () => import('./categorias/subcategorias/productos/productos.module').then( m => m.ProductosPageModule)
  },
  {
    path: 'categorias/:id/subcategorias/:idSubcategoria/productos/agregar',
    loadChildren: () => import('./categorias/subcategorias/productos/agregar/agregar.module').then( m => m.AgregarPageModule)
  },
  {
    path: 'categorias/:id/subcategorias/:idSubcategoria/productos/:idProducto/editar',
    loadChildren: () => import('./categorias/subcategorias/productos//editar/editar.module').then( m => m.EditarPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./checkout/checkout.module').then( m => m.CheckoutPageModule)
  },
  {
    path: 'cordova',
    loadChildren: () => import('./ejememplo-cordova/ejememplo-cordova.module').then( m => m.EjememploCordovaPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { CategoriasService } from 'src/app/servicios/categorias.service';
import { ToastService } from 'src/app/servicios/toast.service';

@Component({
  selector: 'app-ejemplo',
  templateUrl: './ejemplo.page.html',
  styleUrls: ['./ejemplo.page.scss'],
})
export class EjemploPage implements OnInit {
  variable_ejemplo  = null;
  constructor(
    public servicios_categorias:CategoriasService,
    private servicio_toastr:ToastService
  ) { }

  async ngOnInit() {
    let categorias = await this.servicios_categorias.index();
    console.log(this.variable_ejemplo,categorias);
  }

  async guardar(data){
    console.log(data);
    let resultado:any = await this.servicios_categorias.store(data);
    console.log(resultado);
    if(resultado.res){
      this.servicio_toastr.success()
    }
    else{
      this.servicio_toastr.error(resultado.errores);
    }
  }

}

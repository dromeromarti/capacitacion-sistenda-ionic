import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { LoaderService } from '../loader.service';
import { CategoriasService } from '../servicios/categorias.service';
import { SubcategoriasService } from '../servicios/subcategorias.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage {
  categoria_id;
  categorias;
  subcategorias;

  constructor(
    private loader_service: LoaderService,
    private servicio_categorias: CategoriasService,
    private servicio_subcategorias: SubcategoriasService
    ) { }

  async ionViewWillEnter() {
    try {
      await this.loader_service.present();
      this.categorias = await this.servicio_categorias.index();
      console.log('categorias', this.categorias)
      this.categoria_id = this.categorias[0].id;
      this.subcategorias = await this.servicio_subcategorias.index(this.categoria_id);
      await this.loader_service.dismiss();
    } catch(error) {
      console.log('hubo un error', error);
      await this.loader_service.dismiss();
    }
  }

  async getSubcategorias(id) {
    try {
      await this.loader_service.present();
      this.subcategorias = await this.servicio_subcategorias.index(id);
      console.log('subcategorias', this.subcategorias)
      await this.loader_service.dismiss();
    } catch(error) {
      console.log('hubo un error', error);
      await this.loader_service.dismiss();
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Globals } from 'src/app/globals';
import { LoaderService } from 'src/app/loader.service';
import { ProductosService } from 'src/app/servicios/productos.service';
import { SubcategoriasService } from 'src/app/servicios/subcategorias.service';

@Component({
  selector: 'app-detalle-subcategoria',
  templateUrl: './detalle-subcategoria.page.html',
  styleUrls: ['./detalle-subcategoria.page.scss'],
})
export class DetalleSubcategoriaPage {
  subcategoria;
  productos;

  constructor(
    public globals: Globals,
    private route: ActivatedRoute,
    private loader_service: LoaderService,
    private servicio_subcategorias: SubcategoriasService,
    private servicio_productos: ProductosService
    ) { }

  async ionViewWillEnter() {
    try {
      await this.loader_service.present();
      let subcategoria_id = this.route.snapshot.paramMap.get('id');
      this.subcategoria = await this.servicio_subcategorias.get(subcategoria_id);
      this.productos = await this.servicio_productos.index(subcategoria_id);
      await this.loader_service.dismiss();
    } catch(error) {
      console.log('hubo un error', error);
      await this.loader_service.dismiss();
    }
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleSubcategoriaPage } from './detalle-subcategoria.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleSubcategoriaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleSubcategoriaPageRoutingModule {}

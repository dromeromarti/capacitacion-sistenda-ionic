import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleSubcategoriaPageRoutingModule } from './detalle-subcategoria-routing.module';

import { DetalleSubcategoriaPage } from './detalle-subcategoria.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleSubcategoriaPageRoutingModule
  ],
  declarations: [DetalleSubcategoriaPage]
})
export class DetalleSubcategoriaPageModule {}

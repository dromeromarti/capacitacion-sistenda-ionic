import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Globals } from '../globals';
import { LoaderService } from '../loader.service';
import { ProductosService } from '../servicios/productos.service';
import { VentasService } from '../servicios/ventas.service';
import { DetallePagosService } from '../servicios/detalle-pagos.service';

declare let window;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage {
  mostrar_confirmacion = true;
  productos = [];

  constructor(
    public globals: Globals,
    private loader_service: LoaderService,
    private servicio_productos: ProductosService,
    private alert_controller: AlertController,
    private servicio_ventas: VentasService,
    private servicio_pago: DetallePagosService
  ) { }

  async ionViewWillEnter() {
    try {
      await this.loader_service.present();
      await this.inicializaInformacionMostrar();
      await this.loader_service.dismiss();
    } catch(error) {
      console.log('hubo un error', error);
      await this.loader_service.dismiss();
    }
  }

  inicializaInformacionMostrar() {
    this.productos = [];
    this.globals.carrito.forEach(async x => {
      let producto: any = await this.servicio_productos.get(x.id);
      producto.cantidad = x.cantidad;
      this.productos.push(producto);
    })
    console.log('estos son los productos del carrito', this.productos)
  }

  async eliminar(producto_id) {
    if((this.globals.getCantidadCarrito(producto_id) - 1) == 0) {
      // Mostrar una alerta
      await this.alertaConfirmacionEliminar(producto_id);
    } else {
      this.globals.eliminarDeCarrito(producto_id)
    }
  }

  async alertaConfirmacionEliminar(producto_id) {
    const alert = await this.alert_controller.create({
      cssClass: 'my-custom-class',
      header: '¡Atención!',
      message: '¿Quieres quitar este producto de tu carrito de compras?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: async () => {
            console.log('Confirm Okay');
            let index = this.productos.find(x => x.id == producto_id);
            this.productos.splice(index, 1);
            await this.globals.eliminarDeCarrito(producto_id);
          }
        }
      ]
    });

    await alert.present();
  }

  async alertaErrorPaypal() {
    const alert = await this.alert_controller.create({
      cssClass: 'my-custom-class',
      header: '¡Atención!',
      message: 'Hubo un error con la conexión a Paypal y la transacción no fue procesada, vuelve a intentarlo por favor.',
      buttons: [{
          text: 'Entendido',
          handler: async () => {
            console.log('Si entendio');
          }
        }
      ]
    });

    await alert.present();
  }

  async inicializaPaypal() {
    let that = this;
    this.mostrar_confirmacion = false;
    const total = await this.calculaTotalCarrito();
    await window.paypal.Buttons({
      // Set up the transaction
      createOrder: function (data, actions) {
        return actions.order.create({
          purchase_units: [{
            amount: {
              currency: 'MXN',
              value: total,
            }
          }]
        });
      },
      // Finalize the transaction
      onApprove: function (data, actions) {
        return actions.order.capture()
          .then(async function (details) {
            // Guardar en la BD
            console.log('Se proceso paypal con exito, aqui vamos a guardar en la BD',details);
            //Al completar la venta tenemos que guardar el pedido, guardar la informacion de pago y el detalle de ventas
            //aqui vamos a iniciar la vent guardando una venta (asignando el id de venta)
            let respuesta_venta:any = await that.servicio_ventas.store(details);
            let venta_id = respuesta_venta.id;
            console.log(respuesta_venta);


            that.productos.forEach(async x => {
              console.log(x,venta_id)
              x.venta_id = venta_id;
              let respuesta_detalle = await that.servicio_ventas.store_producto(x)
              console.log(respuesta_detalle,x);
            });
            details.venta_id = venta_id;
            //aqui guardamos la informacion de pago
            details.tipo_pago_id = 1;//paypal
            let respuesta_pago = await that.servicio_pago.store(details);
            console.log(respuesta_pago)
            //aqui vamos a borrar el carrito si se guardo correctamente
          })
          .catch(err => {
            console.log("error", err);
          })
      },
      onCancel: (data, actions) => {
        console.log('OnCancel', data, actions);
      },
      onError: async err => {
        console.log('Hubo un error en Paypal', err);
      },
      onClick: (data, actions) => {
        console.log('onClick', data, actions);
      },
    }).render('#paypal-button-container');
  }

  reseteaPaypal() {
    this.mostrar_confirmacion = true;
    document.getElementById('paypal-button-container').innerHTML = '';
  }

  calculaTotalCarrito() {
    let total = 0;
    this.productos.forEach(x => total += x.precio * this.globals.getCantidadCarrito(x.id));
    console.log('total a cobrar: ' + total)

    return total;
  }

}

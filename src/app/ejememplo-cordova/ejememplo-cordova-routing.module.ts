import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EjememploCordovaPage } from './ejememplo-cordova.page';

const routes: Routes = [
  {
    path: '',
    component: EjememploCordovaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EjememploCordovaPageRoutingModule {}

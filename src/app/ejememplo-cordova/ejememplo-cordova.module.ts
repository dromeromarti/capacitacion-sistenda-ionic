import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EjememploCordovaPageRoutingModule } from './ejememplo-cordova-routing.module';

import { EjememploCordovaPage } from './ejememplo-cordova.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EjememploCordovaPageRoutingModule
  ],
  declarations: [EjememploCordovaPage]
})
export class EjememploCordovaPageModule {}

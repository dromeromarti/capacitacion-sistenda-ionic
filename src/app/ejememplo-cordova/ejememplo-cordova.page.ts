import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions, CameraPreviewDimensions } from '@ionic-native/camera-preview/ngx';
import { BatteryStatus } from '@ionic-native/battery-status/ngx';

@Component({
  selector: 'app-ejememplo-cordova',
  templateUrl: './ejememplo-cordova.page.html',
  styleUrls: ['./ejememplo-cordova.page.scss'],
})
export class EjememploCordovaPage implements OnInit {

  constructor(
    private callNumber:CallNumber,
    private geolocation: Geolocation,
    private cameraPreview: CameraPreview,
    private batteryStatus: BatteryStatus
  ) { }

  ngOnInit() {
    // watch change in battery status
    const subscription = this.batteryStatus.onChange().subscribe(status => {
       console.log(status.level, status.isPlugged);
    });
    // this.callNumber.callNumber("4444212235", true)
    //   .then(res => console.log('Launched dialer!', res))
    //   .catch(err => console.log('Error launching dialer', err));

        this.geolocation.getCurrentPosition().then((resp) => {
          console.log(resp);
         // resp.coords.latitude
         // resp.coords.longitude
        }).catch((error) => {
          console.log('Error getting location', error);
        });

  let watch = this.geolocation.watchPosition();
  watch.subscribe((data) => {
   // data can be a set of coordinates, or an error (if an error occurred).
   // data.coords.latitude
   // data.coords.longitude
  });


      //   const cameraPreviewOpts: CameraPreviewOptions = {
      //     x: 0,
      //     y: 0,
      //     width: window.screen.width,
      //     height: window.screen.height,
      //     camera: 'rear',
      //     tapPhoto: true,
      //     previewDrag: true,
      //     toBack: true,
      //     alpha: 1
      //   }
      //   // start camera
      // this.cameraPreview.startCamera(cameraPreviewOpts).then(
      //   (res) => {
      //     console.log(res)
      //   },
      //   (err) => {
      //     console.log(err)
      //   });
      //

  }

}

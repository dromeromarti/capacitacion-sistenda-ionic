import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Globals } from '../globals';
// import { Globals } from '../../globals';
// import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {

  constructor(private http: HttpClient, private globals: Globals) { }

  index() {
    return this.http.get(`${this.globals.url}categorias`).toPromise();
  }

  store(objeto) {
    return this.http.post(`${this.globals.url}categorias`,objeto).toPromise();

  }

  get(id){
    return this.http.get(`${this.globals.url}categorias/${id}`).toPromise();
  }

  update(id,objeto){
    return this.http.put(`${this.globals.url}categorias/${id}`, objeto).toPromise();
  }

  delete(id){
    return this.http.delete(`${this.globals.url}categorias/${id}`).toPromise();
  }

}

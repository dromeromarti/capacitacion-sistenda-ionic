import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DetallePagosService {

  constructor(private http: HttpClient) { }

  store(objeto) {
    return this.http.post('http://localhost:8000/api/detalle-pago',objeto).toPromise();
  }
}

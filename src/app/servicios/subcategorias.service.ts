import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Globals } from '../globals';

@Injectable({
  providedIn: 'root'
})
export class SubcategoriasService {

  constructor(private http: HttpClient, private globals: Globals) { }

  index(id) {
    return this.http.get(`${this.globals.url}subcategorias/subcategorias-de/${id}`).toPromise();
  }

  store(objeto) {
    return this.http.post(`${this.globals.url}subcategorias`, objeto).toPromise();

  }

  get(id){
    return this.http.get(`${this.globals.url}subcategorias/${id}`).toPromise();
  }

  update(id,objeto){
    return this.http.put(`${this.globals.url}subcategorias/${id}`, objeto).toPromise();
  }

  delete(id){
    return this.http.delete(`${this.globals.url}subcategorias/${id}`).toPromise();
  }
}

import { TestBed } from '@angular/core/testing';

import { DetallePagosService } from './detalle-pagos.service';

describe('DetallePagosService', () => {
  let service: DetallePagosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetallePagosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

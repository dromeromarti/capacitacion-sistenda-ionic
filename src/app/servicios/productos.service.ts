import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private http: HttpClient) { }

  index(id) {
    return this.http.get('http://localhost:8000/api/productos/productos-de/'+id).toPromise();
  }

  store(objeto) {
    return this.http.post('http://localhost:8000/api/productos',objeto).toPromise();

  }

  get(id){
    return this.http.get('http://localhost:8000/api/productos/'+id).toPromise();
  }

  update(id,objeto){
    return this.http.put('http://localhost:8000/api/productos/'+id, objeto).toPromise();
  }

  delete(id){
    return this.http.delete('http://localhost:8000/api/productos/'+id ).toPromise();
  }
}

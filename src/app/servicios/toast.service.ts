import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(public toastController: ToastController) { }

  async success() {
    const toast = await this.toastController.create({
      message: 'Exito!',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async error(errores) {
    let mensaje = '';
    const keys = Object.keys(errores);
    for(const x of keys) {
      mensaje += ` ${errores[x]}\n`;
    }
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }
}

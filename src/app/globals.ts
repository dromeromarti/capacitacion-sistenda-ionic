import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class Globals {
  public url = 'http://localhost:8000/api/'; // pruebas
  // public url = 'https://sistenda.com.mx/api/'; // pruebas
  public carrito: any = [];

  constructor(public storage: Storage) {}

  async inicializaCarrito() {
    await this.storage.create();
    let carrito = await this.storage.get('carrito');
    if (!carrito) {
      this.carrito = [];
    } else {
      this.carrito = JSON.parse(carrito);
    }
  }

  existeEnCarrito(producto_id) {
    let res = this.carrito.find(x => x.id == producto_id);

    if(res) return true;
    else return false;
  }

  getCantidadCarrito(producto_id) {
    let producto = this.carrito.find(x => x.id == producto_id);

    if(producto) return producto.cantidad;
    else return 0;
  }

  async agregarACarrito(producto_id) {
    if(this.existeEnCarrito(producto_id)) {
      let producto = this.carrito.find(x => x.id == producto_id);
      producto.cantidad++;
    } else {
      this.carrito.push({
        id: producto_id,
        cantidad: 1
      })
    }
    this.storage.create()
    await this.storage.set('carrito', JSON.stringify(this.carrito));
  }

  async eliminarDeCarrito(producto_id) {
    let producto = this.carrito.find(x => x.id == producto_id);
    if((producto.cantidad - 1) == 0) {
      let indice = this.carrito.indexOf(producto);
      this.carrito.splice(indice, 1);
    } else {
      producto.cantidad--;
    }
    await this.storage.set('carrito', JSON.stringify(this.carrito));
  }

}

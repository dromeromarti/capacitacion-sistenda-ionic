import { Component, OnInit } from '@angular/core';
import { Globals } from './globals';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  public appPages = [
    { title: 'Categorias', url: '/categorias', icon: 'mail' },
    { title: 'Cordova', url: '/cordova', icon: 'mail' },

    { title: 'Outbox', url: '/folder/Outbox', icon: 'paper-plane' },
    { title: 'Favorites', url: '/folder/Favorites', icon: 'heart' },
    { title: 'Archived', url: '/folder/Archived', icon: 'archive' },
    { title: 'Trash', url: '/folder/Trash', icon: 'trash' },
    { title: 'Spam', url: '/folder/Spam', icon: 'warning' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(public globals: Globals) {}

  async ngOnInit() {
    await this.globals.inicializaCarrito();
    console.log('Ya inicialize el carrito');
  }
}
